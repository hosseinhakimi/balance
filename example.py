import balance as blc
import random
import matplotlib.pyplot as plt
import numpy as np
import threading
import time
from multiprocessing import Process
from multiprocessing.pool import ThreadPool

# p_count = 500

# start = time.time()


def partOFIt(TPart,oo):
    _T = np.arange(TPart - 3, TPart, 0.2)
    N = len(_T)
    _E = []
    for T in _T:
        # print(T)
        E = []
        for i in range(10):
            nblc = blc.TriangularBalance(25, 1, 1, 0, T)
            nblc.TriadDynamics(4)
            E.append(nblc.Energy)
        _E.append(np.mean(E))
    aE = np.asarray(_E)
    final = np.concatenate((_T.reshape(N,1),aE.reshape(N,1)),axis = 1)
    np.savetxt('res-' + str(TPart),final)

prcesess = 8
for i in range(prcesess):
    # print(i)
    X = Process(target=partOFIt, args=(3*(i+1),2))
    # print(i+1)
    X.start()
    time.sleep(2)
# partOFIt(3)
